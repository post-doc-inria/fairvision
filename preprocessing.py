import tensorflow as tf
import albumentations as A

AUGMENTATIONS = A.Compose(
    transforms=[
        A.HorizontalFlip(),
        A.HueSaturationValue(),
        A.RandomScale(scale_limit=[0.5, 1.1])
    ],
    bbox_params=A.BboxParams(format='albumentations',
                             label_fields=['class_labels'])
)


def read_line(line):
    splits = tf.strings.split(line, sep=',')
    filename, xmin, ymin, xmax, ymax = splits
    image = read_image(filename)
    xmin = tf.cast(xmin, tf.float32)
    ymin = tf.cast(ymin, tf.float32)
    xmax = tf.cast(xmax, tf.float32)
    ymax = tf.cast(ymax, tf.float32)
    image_shape = tf.shape(image)
    image_height = tf.cast(image_shape[0], tf.float32)
    image_width = tf.cast(image_shape[1], tf.float32)
    xmin /= image_width
    ymin /= image_height
    xmax /= image_width
    ymax /= image_height
    bbox = tf.stack(
        [
            xmin,
            ymin,
            xmax,
            ymax
        ],
        axis=1
    )
    class_labels = tf.constant([1.0], dtype=tf.float32)
    transformed_result = tf.py_function(compute_transform, inp=[
        image,
        bbox,
        class_labels
    ],
                                        Tout=[
                                            tf.uint8,
                                            tf.float32,
                                            tf.float32
                                        ]
                                        )
    image_shape = tf.shape(transformed_result[0])
    xmin *= tf.cast(image_shape[1], tf.float32)
    ymin *= tf.cast(image_shape[0], tf.float32)
    xmax *= tf.cast(image_shape[1], tf.float32)
    ymax *= tf.cast(image_shape[0], tf.float32)
    xmin = tf.cast(xmin, tf.int64)
    ymin = tf.cast(ymin, tf.int64)
    xmax = tf.cast(xmax, tf.int64)
    ymax = tf.cast(ymax, tf.int64)

    pass


def compute_transform(img, bounding_box, cls_lbls):
    transformed = AUGMENTATIONS(
        image=img.numpy(),
        bboxes=bounding_box.numpy(),
        class_labels=cls_lbls.numpy()
    )
    return transformed['image'], transformed['bboxes'], transformed[
        'class_labels']


def read_image(filename):
    image = tf.keras.preprocessing.image.load_img(
        path=filename
    )
    return image


def single_box_mask(bbox, image_height, image_width):
    box = tf.cast(bbox, tf.int32)
    image_height = tf.cast(image_height, tf.int32)
    image_width = tf.cast(image_width, tf.int32)
    xmin, ymin, xmax, ymax = tf.unstack(box, num=4, axis=-1)
    xmin = tf.squeeze(xmin)
    ymin = tf.squeeze(ymin)
    xmax = tf.squeeze(xmax)
    ymax = tf.squeeze(ymax)
    left_portion = tf.zeros(
        shape=(
            image_height,
            xmin
        ),
        dtype=tf.bool
    )

    right_portion = tf.zeros(
        shape=(
            image_height,
            image_width - xmax
        ),
        dtype=tf.bool
    )
    middle_top_portion = tf.zeros(
        shape=(
            ymin,
            xmax - xmin
        ),
        dtype=tf.bool
    )
    box_portion = tf.ones(
        shape=(
            ymax - ymin,
            xmax - xmin
        ),
        dtype=tf.bool
    )
    middle_bottom_portion = tf.zeros(
        shape=(
            image_height - ymax,
            xmax - xmin
        ),
        dtype=tf.bool
    )
    middle_portion = tf.concat(
        [
            middle_top_portion,
            box_portion,
            middle_bottom_portion
        ],
        axis=0
    )
    box_mask = tf.concat(
        [
            left_portion,
            middle_portion,
            right_portion
        ],
        axis=1
    )
    return box_mask
