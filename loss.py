import tensorflow as tf
from model import AXIS


def deepball_loss(y_true, y_pred):
    loss = tf.keras.losses.BinaryCrossentropy(
        from_logits=True
    )(y_true, y_pred)
    return loss


def compute_confidence_map(y_pred):
    conf_map = tf.keras.layers.Softmax(
        axis=AXIS
    )(y_pred)
    if AXIS == -1:
        ball_confidence = conf_map[:, :, :, 1]
        bg_confidence = conf_map[:, :, :, 0]
    else:
        ball_confidence = conf_map[:, 1, :, :]
        bg_confidence = conf_map[:, 0, :, :]

    output = dict(
        ball_confidence=ball_confidence,
        bg_confidence=bg_confidence
    )
    return output
