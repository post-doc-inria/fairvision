import tensorflow as tf


def train_dataset(training_file):
    db = tf.data.TextLineDataset(filenames=training_file)
    db = db.shuffle(buffer_size=10, reshuffle_each_iteration=True)
    pass
