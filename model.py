import tensorflow as tf

AXIS = -1 if tf.keras.backend.image_data_format() == 'channels_last' else 1


class ConvBNormLayer(tf.keras.layers.Layer):
    def __init__(self,
                 kernel_size,
                 num_filters,
                 stride=None,
                 name=None
                 ):
        super(ConvBNormLayer, self).__init__(name=name)
        if stride is None:
            stride = [1, 1]
        self._conv1 = tf.keras.layers.Conv2D(
            filters=num_filters[0],
            kernel_size=kernel_size[0],
            strides=stride[0],
            padding='same',
            use_bias=False
        )
        self._bn1 = tf.keras.layers.BatchNormalization(
            axis=AXIS
        )
        self._conv2 = tf.keras.layers.Conv2D(
            filters=num_filters[1],
            kernel_size=kernel_size[1],
            strides=stride[1],
            padding='same',
            use_bias=False
        )
        self._bn2 = tf.keras.layers.BatchNormalization(
            axis=AXIS
        )

    def call(self, input_batch, **kwargs):
        out = self._conv1(input_batch)
        out = self._bn1(out)
        out = tf.keras.layers.Activation(activation='relu')(out)
        out = self._conv1(out)
        out = self._bn1(out)
        out = tf.keras.layers.Activation(activation='relu')(out)
        return out


class DeepBallModel(tf.keras.Model):
    def __init__(self, name='DeepBallModel'):
        super(DeepBallModel, self).__init__(name=name)
        self._conv1 = ConvBNormLayer(
            kernel_size=[7, 3],
            num_filters=[8, 8],
            stride=[2, 1],
            name='Conv1'
        )
        self._conv2 = ConvBNormLayer(
            kernel_size=[3, 3],
            num_filters=[16, 16],
            name='Conv2'
        )
        self._conv3 = ConvBNormLayer(
            kernel_size=[3, 3],
            num_filters=[32, 32],
            name='Conv3'
        )
        self._maxpool = tf.keras.layers.MaxPool2D(
            pool_size=2,
            padding='same'
        )
        self._conv4 = ConvBNormLayer(
            kernel_size=[3, 3],
            num_filters=[56, 2],
            name='Conv4'
        )

    def call(self, input_batch, **kwargs):
        conv1_out = self._conv1(input_batch)
        conv1_out = self._maxpool(conv1_out)
        conv2_out = self._conv2(conv1_out)
        conv2_out = self._maxpool(conv2_out)
        conv3_out = self._conv3(conv2_out)
        conv3_out = self._maxpool(conv3_out)
        conv2_out_upsample = tf.keras.layers.UpSampling2D()(conv2_out)
        conv3_out_upsample = tf.keras.layers.UpSampling2D()(conv3_out)
        conv4_inp = tf.keras.layers.Concatenate(axis=AXIS)(
            [
                conv1_out,
                conv2_out_upsample,
                conv3_out_upsample
            ]
        )
        conv4_out = self._conv4(conv4_inp)
        return conv4_out
